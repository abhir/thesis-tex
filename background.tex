\chapter{Background}\label{chap:background}

In this chapter, we will discuss the programming model of Intel's integrated graphics and the details of the design and implementation of GVT-g relevant to our work.

\section{Intel GPU Programming Model}

\begin{figure}
\tikzset{
module/.style={rounded corners, font=\sffamily, thick},
text module/.style={module, top color=blue!10, bottom color=blue!20, draw=blue!75, minimum height=3em},
up arrow/.style={module arrow, shape border rotate=90},
down arrow/.style={module arrow, shape border rotate=-90},
module arrow/.style={single arrow, single arrow head extend=0.5em, draw=gray!75, inner color=gray!10, outer color=gray!30, thick, shape border uses incircle, anchor=tail, minimum height=2em}
}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\centering
\begin{tikzpicture}

\node [text module, minimum width=20em] (cpu) {CPU};
\node [text module, below=4em of cpu, anchor=east, minimum width=10em] (cb) {Command Buffer};
\node [text module, right=0.1em of cb, minimum width=10em] (fb) {Frame Buffer};
\node [font=\sffamily, below=5.5em of cpu] (lmem) {\textbf{System Memory}};
\node [text module, below=2.5em of lmem, minimum width=20em] (gpu) {GPU Page Tables};
\node [text module, below=3.5em of gpu, anchor=east, minimum width=10em] (re) {Render Engine};
\node [text module, right=0.1em of re, minimum width=10em] (de) {Display Engine};
\node [font=\sffamily, below=5.0em of gpu] (lgpu) {\textbf{GPU}};

\node [down arrow, below=0em of cpu, label={right: \textbf{\ Program}}] {};
\node [up arrow, above=0.5em of gpu, label={right: \textbf{\ Fetch}}] {};
\node [up arrow, above=0em of re, label={right: \textbf{\ Memory Access}}] {};
\node [up arrow, above=0em of de] {};

\begin{pgfonlayer}{background}
    \path (cpu.north west)+(-0.5,0.5) node (e) {};
    \path (lgpu.south -| gpu.east)+(+0.5,-0.4) node (f) {};
    \path[fill=blue!5,rounded corners, draw=black!50, dashed] (e) rectangle (f);

    \path (cb.west |- fb.north)+(-0.2,0.2) node (a) {};
    \path (lmem.south -| fb.east)+(+0.2,-0.0) node (b) {};
    \path [fill=yellow!10,rounded corners, draw=black!50, dashed] (a) rectangle (b);

    \path (gpu.north west)+(-0.2,0.2) node (c) {};
    \path (lgpu.south -| gpu.east)+(+0.2,-0.1) node (d) {};
    \path[fill=yellow!10,rounded corners, draw=black!50, dashed] (c) rectangle (d);
\end{pgfonlayer}

\end{tikzpicture}
\caption{Architecture of Intel's Integrated GPU.}
\label{fig:igpu-arch}
\end{figure}

Figure \ref{fig:igpu-arch} shows the general architecture of Intel's integrated graphics\cite{gvirt}.  Unlike discrete GPUs which have dedicated memory, the integrated GPUs use part of the system memory as graphics memory.  The GPU page tables map the system memory into GPU virtual address space.  The \textit{render engine} is responsible for fetching and executing commands from the command buffer (stored in a ring buffer) and the \textit{display engine} is responsible for the fetching pixel data from the frame buffer and sending it to the external display device.

The CPU and the GPU operate in a producer-consumer model.  User applications use high-level graphics programming APIs such as OpenGL and Direct3D, which the graphics driver converts into hardware specific commands.  The commands are written into the \textit{command buffer} which includes a primary buffer and a batch buffer.  Each \textit{engine} has its own primary buffer.

\textbf{Command buffer:} The GPU commands are submitted through the command buffer (also called primary buffer) which is a ring buffer. This process is called ``command submission''.  The GPU driver is responsible for maintaining this ring buffer.  It controls a register tuple \textit{(head, tail)}; the CPU command submission process updates the \textit{tail} pointer while the GPU command completion process updates the \textit{head} pointer.

The Linux DRI (Direct Rendering Infrastructure) model provides the userspace processes with direct access to the GPU. The role of GPU driver in this scenario is that of a ``mediator''.  The userspace processes submit both commands and the corresponding data in a \textit{batch buffer}.  The command buffer is limited in size and under the control of the GPU driver, while the batch buffer is created in the userspace and doesn't have any hardware imposed restrictions.  The batch buffer is invoked indirectly from the command buffer during command submission.

\section{Design and architecture of GVT-g}

\begin{figure}
\centering
% TODO re-do the diagram in Tikz?
\includegraphics[width=\textwidth*3/4]{gvirt-arch.png}
\caption{GVT-g Architecture.  \textit{Source:} \cite{gvirt}}
\label{fig:gvirt-arch}
\end{figure}

Figure \ref{fig:gvirt-arch} shows the architecture of GVT-g (gVirt).  GVT-g is based on the Xen hypervisor.  Xen doesn't support dual mode of operation i.e. both mediation and pass-through.  Hence, a gVirt stub is used to have selective policies for handling trap-and-forward and pass-through.  Dom0 is the privileged VM in Xen.  It contains the gVirt mediator and the QEMU device model required for VGA emulation.  Each VM uses its own native driver for communicating with the vGPU model provided by the mediator.  The mediator is implemented as a Linux kernel module and is loaded with and initialized by the native graphics driver (i915) of dom0.  It is responsible for maintaining the vGPU device models presented to each VM and for coordinating their execution on the physical GPU.

The scheduler is implemented as part of the mediator driver.  Each VM is provided direct access to the physical GPU for execution and it is the scheduler's job to mediate it among the VMs.  The vGPU model consists of the I/O registers and the internal state of the GPU, which can be saved and restored through GPU commands.  To do a context switch among the VMs, these states must be saved and restored along with flushing of caches and TLB entries.

The VMs are provided access to their ring buffers.  However, since it is not safe to directly execute commands from the guest ring buffers, they are audited before they are sent to the GPU for execution.  This involves maintaining a separate shadow ring buffer which the commands are copied to, after they've been audited.  The I/O registers are not pass-through and hence, the updates made to the \textit{tail} register are trapped to the driver which then performs the auditing of the commands before copying them to the shadow buffer.  The (shadow) ring buffers are switched before and after each context switch.

\chapter{Introduction}\label{chap:intro}

The graphics processing unit (GPU), originally designed for accelerating graphics applications such as video games and video playback, has found its way into general purpose computing.  Owing to its highly parallel and energy-efficient design it has replaced CPU in many distinct use cases.  It is being actively used in modern computing systems for accelerating windowing systems, web applications, and office productivity suites as well as in domains such as image processing, computer aided designs (CAD), data processing and machine learning.  APIs such as CUDA, OpenCL, C++AMP etc.\ are designed to take advantage of the parallel computing power of the GPU.

Virtual Desktop Infrastructure (VDI) or Desktop Virtualization is a virtualization technology that provides centralized delivery and management of desktop environment.  It allows desktop environments to be installed and managed in a centralized server pool and then be used remotely using thin-client systems e.g.\ laptops, tablets, etc.  A virtualized infrastructure allows easy management, enhanced security, efficient utilization, and scaling.

In order to incorporate GPUs into VDI, it must be virtualized.  The demand for its virtualization has led to development of new virtualization technologies.  These technologies vary in performance, features, and sharing capability.  The following section discusses them, considering both their advantages and disadvantages.


\section{GPU virtualization}

A virtualization system is composed of layers of software systems.  At the bottom of it is the hypervisor or the virtual machine monitor (VMM).  The hypervisor has direct control over the hardware and is responsible for time multiplexing it among the virtual machines (\Fref{tab:virt_arch}).

\begin{table}[]
    \centering

    \begin{tabular}{|c|c|}
    \hline
    Virtual Machine (VM) & Virtual Machine (VM) \\
    \hline
    \multicolumn{2}{|c|}{Hypervisor / VMM} \\
    \hline
    \multicolumn{2}{|c|}{Hardware} \\
    \hline
    \end{tabular}

    \caption{Type-1/Bare metal virtualization}\label{tab:virt_arch}
\end{table}

From the viewpoint of the hypervisor, the GPU is primarily an I/O device.  Therefore, the techniques used for I/O virtualization are applicable here.  In approximate order of increasing performance, the techniques used for I/O (or GPU) virtualization as follows.

\begin{enumerate}
    \item \textbf{Device emulation:}
    Highly complex devices such as GPU are extremely difficult to emulate while maintaining acceptable levels of performance, so it is not used in the case of GPUs.
    \item \textbf{API forwarding}
    or API remoting involves two drivers, one on the guest VM and one on the host hypervisor.  The guest driver forwards API calls (OpenGL, CUDA, etc.) to the host driver via hypercalls or similar mechanisms and the host driver passes them to GPU for acceleration.
    \newline
    This approach requires modification of the guest graphics software stack and hence, maintaining compatibility and feature completeness becomes a non-trivial task as new functionalities and features are added into the API libraries.  However, the performance is relatively good with only overhead coming from call forwarding.  Sharing is also easily achievable as the host driver is the only owner of the GPU and can be used for enforcing any sharing policy.
    \item \textbf{Direct pass-through} reserves a device for a single VM.  Using the pass-through technology, it is possible to dedicate a device to a VM.  The device is directly managed by the guest OS providing maximum possible performance at the cost of sharing.
    \item \textbf{Mediated pass-through} passes through performance-critical portions of the device, while mediating access to privileged operations.  The device does not belong to a single VM.  This ensures sharing capability while keeping good performance with full features of guest OS.
\end{enumerate}

Mediated pass-through enables sharing of GPU with acceptable performance and feature completeness.  Hence, it provides an appropriate foundation for implementing VDI.


% \section{A fully virtualized, mediated pass-through implementation: Intel GVT-g}
\section{Intel GVT-g GPU Virtualization}

Intel GVT-g (\textit{formerly gVirt})\cite{gvirt} is a full GPU virtualization implementation utilizing mediated pass-through.  It is based on Xen\cite{xen} (there's a KVM port as well) hypervisor and requires at least a 4th generation Intel integrated GPU.  It combines full virtualization, i.e.\ a native graphics driver in guest with mediated pass-through allowing good performance with secure isolation among guests.
% We will discuss its design and architecture in later sections (\ref{chap:background}).

A VM under GVT-g is provided a virtual GPU (vGPU) which behaves identical to a physical one and hence, allows an unmodified graphics driver to be used with it.  A single physical GPU can be shared by up to 7 vGPUs.  At a given time, there can be only one user of the GPU (a \textit{user} has an executing context on the GPU).  Access is time-shared among the VMs by context switching from one VM to another.  A context switch involves saving state of current user and restoring state of new user.  Hence, each VM is able to present an illusion of a fully-featured GPU to its user.


\section{vGPU Scheduling}

Similar to CPU virtualization, GPU virtualization demands scheduling of virtual GPUs (vGPUs).  However, there are some properties of the GPU which make the task different from regular vCPU scheduling.  These are discussed below.

\begin{itemize}
    \item \textbf{No preemption:} Unlike a CPU, current GPUs cannot be preempted.  This means that the real-time guarantees, like response time, cannot be achieved.

    \item \textbf{High context switch penalty:} GPU context switch cost is very high compared to CPU (\textasciitilde{}1000x, according to \cite{gvirt}).  A large context switch overhead prohibits frequent context switches.

    \item \textbf{Driver timeout:} Current operating systems allow direct access to GPU from user space.  This involves setting up and restoring GPU state, whenever appropriate, by user level code and a lock is used for gaining exclusive access.  However, a misbehaving process can, intentionally or otherwise, hold the lock for extended periods of time thereby starving other processes, which include critical applications like windowing systems.
    \newline
    To prevent this, modern GPU drivers have a timeout detection functionality\cite{wddm-tdr}.  If the GPU driver detects that the last GPU command submission is taking too much time, it will try to reset the GPU assuming a GPU hang has occurred and a reset is required.
    \newline
    This creates an interesting situation where a guaranteed response time is required by the vGPU which, otherwise, will try to reset the GPU.  The GPU reset (both virtual and physical) is an expensive operation and degrades the overall performance of the system.
\end{itemize}


To be able to distribute the GPU resources in a predictable manner, certain quality of service (QoS) attributes are desirable.

A \textbf{work conserving} scheduler always tries to keep the scheduled resource busy, if there are submitted jobs ready to be scheduled.  This is in contrast to a \textbf{non-work conserving scheduler} that, in some cases, may leave the scheduled resource idle despite the presence of jobs ready to be scheduled.\cite{wiki-wc}.  A work conserving scheduler tries to optimize the overall throughput of the resource.

Resource control methods such as \textbf{proportional share allocation} are used to enforce isolation and control allocation. Each VM is associated with an attribute called \textit{weight} or \textit{share} which dictates how much share of the resource it will receive.  \textit{Weight} is a relative allocation measure i.e.\ actual resource allocation is decided by the ratio of the given value to the summation or total of all values.  The objective is to provide isolation among the users.


\section{Contribution of this Thesis}

In this thesis, we present the design and prototype implementation of two GPU scheduling algorithms, one designed for providing isolation at the cost of some throughput and another for maximizing throughput at the cost of predictability and isolation.  We test them for different workloads and analyze the results for gaining better insights.

\section{Organization of this Thesis}

In Chapter \ref{chap:background}, we cover the background useful for better understanding of the thesis.  In Chapter \ref{chap:related} we discuss the related work.  In Chapter \ref{chap:design-impl}, we present our design and the details of implementation of our techniques.  In Chapter \ref{chap:experiment}, we measure and analyze the performance of our implementation through experimental data.  We conclude in Chapter \ref{chap:conclusions} providing the summary of this work and directions for future work.

\chapter{Design and Implementation}
\label{chap:design-impl}

\section{Design Goals and Assumptions}

\subsection*{Goals:}

\begin{enumerate}
\item \textbf{Resource allocation control} in the form of \textit{share} or \textit{weight}.  A VM should get the GPU resource proportional to its \textit{weight}.  This is called ``weighted-fairness''.  We use it to enforce isolation among the vGPUs.
\item \textbf{Throughput vs responsiveness trade-off:} Have two separate modes of operation: one for maximizing throughput and another for maximizing responsiveness.
\item \textbf{Respect driver timeout:} Driver timeout can result in vGPU reset which can affect the stability of the VM and degrade the system's overall throughput.  It is not possible to avoid the timeout altogether, since due to the non-preemptive nature of the GPU, a command once submitted cannot be stopped midway and a VM can launch a GPU kernel, which can potentially run forever.
\end{enumerate}

\subsection*{Assumptions:}

\begin{enumerate}
\item \textbf{No preemption:} Current generation GPUs do not support GPU preemption.  Hence, we base our design on this assumption.
\item \textbf{Interactive workload with driver timeout:} We target VDI workloads i.e. desktop applications which submit commands in small batches and expect short response times, failing which the driver would timeout.  Batch workloads such as OpenCL kernels which may execute for longer periods of time are \textit{not} our primary target.
\end{enumerate}

\section{General Algorithm}

We start with a simple round-robin algorithm already implemented in GVT-g.  Before moving to policy enforcement, we will talk about the routines common to both algorithms.
\newline
\textbf{vGPU selection:}
\newline
In every round, the next vGPU is selected on the basis of two factors:
\begin{itemize}
\item \textbf{Budget:} It can be \textit{on-GPU time} i.e. the amount of time a vGPU is allowed to get exclusive access to GPU, or the number of \textit{commands} it is allowed to submit to the GPU.  A ``command'' in this context refers to an instruction written into the ring buffer of a GPU engine.  A command can be as simple as logical NOT to as complex as a primitive of a high level API such as Direct3D or OpenGL.  Budget can go negative in the case of on-GPU time when the scheduled time of a vGPU exceeds the time it was allocated.

\item \textbf{State of its ring buffer:} Since each vGPU has its own pass-through command buffer, it can push commands in to it even when the vGPU is not scheduled.  This way the commands get accumulated until the vGPU gets access to the GPU.  The state of a ring buffer is defined by a tuple of registers \textit{(head, tail)}; by reading the value of these registers, one can tell whether there are commands available for execution i.e. when $head \neq tail$.
\end{itemize}

We have another factor which is used as a \textit{special} case: \textbf{last schedule-out time}.  Drivers trigger TDR (Timeout Detection and Recovery) when a submitted command takes too much time to execute.  To prevent the timeout, we track the time when the VM got the last access to the GPU.  So, before the usual round-robin selection, we check if a VM is about to trigger TDR by checking the difference between current time and last schedule-out time, and then appropriately decide if it should be scheduled-in.
\newline
\textbf{Budget (re-)allocation:}
\newline
Budget is allocated in a proportional-share manner:
$$
Budget, B = constant \times \frac { weight_i } { \sum_{i=1}^{n} weight_i }
$$
Budget replenishment takes two forms, one where we add the calculated budget $B$ to the budget of the VM thus taking into account previous debt/excess and the other where we do not.  The former form is necessary as in the event a VM ``hijacks'' the GPU for too long exceeding its allocated time budget in one round of allocation, it can be ``punished'' in the next round by taking into account its debt from the previous round while simultaneously pushing the disadvantaged vGPUs ahead by adding their leftover budget.  This scheme helps in maintaining the weighted-fairness.
\newline
\textbf{Time quantum calculation:}
\newline
Scheduler time quantum is the time between two successive calls to the scheduler.  Effectively, it is the time between two context switches (if the first schedule call triggers a context switch).  In the case of a GPU, due to large context switch time and lack of preemption, instead of using a fixed time quantum we factor the VM's remaining time budget into calculation:
$$
Sleep time, S = clamp(T_i, schedulerThreshold, timerPeriod)
$$
where, $T_i$ is the time budget of the vGPU i.e. the value we are clamping, $schedulerThreshold$ is its minimum value and $timerPeriod$ is the maximum.  $clamp$ is defined as:
$$
clamp(x, m, M) = max(m, min(x, M))
$$
\newline
Both $schedulerThreshold$ and $timerPeriod$ are configurable parameters, set to $2 ms$ and $10 ms$ by default.  For a comparison, the context switch overhead of a $4^{th}$ gen Intel GPU is around $700 \mu{s}$.

\section{Scheduling Policies}

We designed and implemented two scheduling policies following the general method described in the previous section.
\subsection*{Time-based:}
In time-based scheduling, budget in the form of on-GPU time is allocated to each VM  which can be spent in quanta governed by the time quantum calculation.  The budget is calculated as
$$
Time slice, ts_i = \left( balancePeriod \times timerPeriod \right) \times \frac { w_i } { \sum_{i=0}^{n} w_i }
$$
where $balancePeriod$ is the time period of budget balancing (in units of scheduler calls) and $timerPeriod$ is the maximum time quantum.

Hence, a total time of $balancePeriod \times timerPeriod$ is divided among the VMs on the basis of their weights.

Time-based scheduling favors predictable response time.  However, some workloads may cause loss in throughput.  Applications which cannot keep the GPU busy throughout their time quantum fall under this category.  We enforce the time limit by preventing command submission once the time quantum has expired.

\subsection*{Command-based:}

In command-based scheduling, we assign a command budget to each VM.  This command budget is per GPU engine (or associated ring buffer).  The number of commands allowed in one round is different for each engine and is parameterized.  Budget calculation is similar to the time-based method with $timerPeriod$ being swapped out for commands allowed:
$$
Commands\ Allowed, C_i = \left( balancePeriod \times cmdsAllowed(ring) \right) \times \frac { w_i } { \sum_{i=0}^{n} w_i }
$$
where $balancePeriod$ is same as earlier.
\newline
The number of commands allowed in one round is calculated as:
$$
clamp(C_i, 5, cmdsAllowed).
$$
Note that this calculation is per ring.  We allow a minimum of 5 commands even if that ring does not have budget for that.  This is done to prevent TDR.
Another thing to be noted is that a VM is \textit{not} allowed to submit commands even when it is scheduled-in.  We enforce this by buffering commands per engine and submitting them at the end of the scheduler call.

Since we are scheduling by commands, to prevent GPU idle time, a context switch needs to happen as soon as it becomes idle.  For this a notification mechanism is required.  We make use of the ring notification interrupts for this.  A batch buffer signals its completion via an interrupt to the CPU.  This interrupt is caught by Xen and forwarded to the driver.  By putting a callback in the interrupt handler, we can detect when a batch buffer has finished execution and hence, can signal the scheduler from there.
However, this approach does not work for non-batch buffer submissions.  For handling this case, we keep a timer which calls the scheduler after a certain timeout.

Command-based scheduling is \textit{work-conserving} (time-based is not).  It favors high throughput over predictable responsiveness.  By switching context as soon as command execution finishes, we minimize GPU's idle time.  However, due to variable cost of each command, we do not get an upper bound on execution time.
This also questions the very idea of fairness in such a context.  However, our experiments suggest that this does not matter much as things get averaged out after some time.

\subsection*{Implementation notes}

The scheduler is part of the mediator kernel module.  We modified the basic round-robin scheduler that was already implemented in the module and added code for supporting command-based scheduling.  We use \textit{sysfs}\cite{sysfs} for runtime parameter passing and re-assignment.

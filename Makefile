all:
	latexmk -xelatex -shell-escape main.tex

clean:
	latexmk -C

.PHONY: all, clean

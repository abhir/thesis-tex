\documentclass[12pt,xetex]{beamer}

\usetheme{Copenhagen}
\usecolortheme{rose}
\usepackage{graphicx}
\usepackage{biblatex}
\bibliography{defence.bib}

\title{On Scheduling of Fully Virtualized GPUs on Integrated Graphics Hardware}
\date{\today}
\author{Abhishek Rose}
\institute{\textit{Supervisors: Dr Mainak Chaudhuri \& Dr Sumit Ganguly}\newline\newline
Department of Computer Science and Engineering\\IIT Kanpur}

% put TOC at the beginning of each section
\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}
%gets rid of top and bottom navigation bars
\setbeamertemplate{footline}[page number]{}
\setbeamertemplate{headline}{}
%gets rid of navigation symbols
\setbeamertemplate{navigation symbols}{}

%%%%%%%%%%%%%%%%%
%%%%% BEGIN %%%%%
%%%%%%%%%%%%%%%%%

\begin{document}

\frame{\titlepage}

\begin{frame}{Outline}
\tableofcontents[]
\end{frame}

%% Background %%
\iffalse

Programming model of GPU:
* command submission:
	* ring buffer -- only primary buffer not batchbuffers (for now)
	* ring registers -- head, tail
	* asynchronous submission model
* non-preemptive nature of command execution

GVT-g model: briefly describe the model
* full virtualization
* mediated passthrough
* shadowing, command parsing

Abstract GPU model presented by GVT-g:
* ring buffer registers
* command submission/execution (shadowing etc.)
* driver-side issues: TDR
\fi

\section{Introduction}
% Include very brief (and high level) introduction to problem
% use terms without defining them (we'll define them precisely later)

% next slide
\subsection{Motivation}
\begin{frame}[fragile]{Motivation}
\begin{itemize}
\item Intel GVT-g \cite{gvirt} is a GPU virtualization technology for full virtualization of Intel's integrated GPUs.
\item Full virtualization $\implies$ Each virtual machine (VM) gets its own virtual GPU (vGPU).
\item For multiplexing multiple vGPUs on to a physical GPU we need a scheduler.
\end{itemize}
\end{frame}
% define the problem statement: can use bullet points for listing topics
% precisely define who do you mean by each term
% present the assumptions, requirements, and challenges
% fluidly move to next slide for covering the required background
\subsection{Assumptions}
\begin{frame}[fragile]
\frametitle{Assumptions}
\begin{itemize}
\item \textbf{No preemption}: An executing GPU kernel cannot be preempted.
\item \textbf{Interactive workload} $\implies$ Handle driver timeout (TDR).
\end{itemize}
\begin{block}{Timeout Detection \& Recovery (TDR)\cite{wddm-tdr}}
Graphics driver keeps a ``wait'' period (the TDR timeout) for each command submission.
\newline
If the execution takes more time than this, the graphics driver deems the GPU as \textit{hung} and tries to ``reset'' it.
\end{block}
\end{frame}

\subsection{Requirements}
\begin{frame}[fragile]{Requirements}
\begin{itemize}
\item \textbf{Resource allocation control} with``weighted-fairness'':
	\begin{itemize}
	\item Define \textit{share} or \textit{weight}.
	\item Allocate GPU resource $\propto weight$.
	\item Enforces \textit{isolation} among the vGPUs.
	\end{itemize}
	\item \textbf{Two (orthogonal) modes of operation}
	\begin{itemize}
	\item Maximize throughput.
	\item Maximize responsiveness.
	\end{itemize}
\item \textbf{Respect driver timeout:} Driver timeout causes GPU reset $\implies$ degrades system's throughput.
\end{itemize}
\end{frame}

% next slide
%% Scheduling %%
\section{Design \& Implementation}

\subsection{Scheduling attributes}
\begin{frame}[fragile]{Attributes}
Scheduling attributes of a vGPU:
\begin{itemize}
	\item execution \textbf{weight ($w_i$)}:
	relative measure of GPU resource allocation.  Higher value $\implies$ more GPU resource.
	\item \textbf{budget}: Could be either of
	\begin{itemize}
		\item \textbf{time slice ($ts_i$):}
		total time allocated to vGPU in one "cycle" (of budget allocation).
		\item \textbf{command budget ($b_i$):}
		\# of commands a vGPU is allowed to submit in one "cycle".
	\end{itemize}
	\item \textbf{last schedule-out timestamp}: schedule a vGPU immediately (out-of-order) if its graphics driver is about to trigger TDR.
\end{itemize}
\end{frame}

\subsection{Algorithm}
\begin{frame}[fragile]{Algorithm}
Based on \textbf{Deficit Round Robin}\cite{DRR}, regular round robin with ``budget carry forward'':
\begin{itemize}
\item Accumulate budget across allocations.
\item Excess/deficit from last round is added into the current budget.
\item Reset to zero once in a while to reset fairness.
\end{itemize}
\begin{exampleblock}{}
Lack of preemption means we cannot guarantee exact schedules.  Over (or under) uses are taken care of using this method.\\
Proportional (weighted) fairness is achieved by combining DRR with \textit{weight-based} budget allocation scheme.
\end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Algorithm}
Steps:
\begin{enumerate}
\item If required, allocate budget.
\item If a vGPU is about to trigger TDR, select it.
\item Else, pick the next ready vGPU in round-robin order.
\item Assign budget for this cycle
\item Schedule the selected vGPU and go to sleep.
\end{enumerate}
\begin{block}{Implementation Note}
The scheduler is invoked by a kernel thread which is triggered (periodically) by a timer function or (on demand) by virtual interrupt handlers.
\end{block}
\end{frame}

\subsection{Scheduling policy}
\begin{frame}[fragile]{Scheduling policy}
Budget type dictates the policy used for allocation.
\begin{itemize}
\item Using \textbf{timeslice} as budget $\implies$ Time-based scheduling.
\item Using \textbf{GPU commands} as budget $\implies$ Command-based scheduling.
\end{itemize}
\end{frame}

\subsection{Timeslice-based scheduling}

\begin{frame}[fragile]{Timeslice-based scheduling}
\begin{itemize}
\item Each vGPU gets a \textit{slice} of GPU's time; call it its \textit{timeslice ($ts_i$)}.
\item A vGPU is allowed to spend its timeslice in bounded chunks/quanta.
\item No context switch before the time quantum expires.  \textit{Even when the GPU becomes idle.}
\begin{exampleblock}{}
Regular behavior $\implies$ consistent response times.
\end{exampleblock}
\begin{alertblock}{}
Allowing idle time $\implies$ loss of throughput.
\end{alertblock}
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Timeslice-based scheduling -- Timeslice allocation}
\textbf{Proportional allocation scheme:}\newline
Allocate timeslice, $ts_i$, in proportion to vGPU's weight, $w_i$:
\begin{equation}
ts_i = \left(allocationPeriod \right) \times \frac { w_i } { \sum_{i=0}^{n} w_i }
\end{equation}
where,
\\
$allocationPeriod$ is the time period for budget allocation,
\begin{exampleblock}{}
Weights are relative measure of allocation.  Set of vGPUs with weights $(10,20,30)$ will behave in similar manner as $(5,10,15)$
\end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Timeslice-based scheduling -- Wait/Sleep time calculation}
$timerPeriod$ is the maximum time we wait before invoking the scheduler.
\\
$ctxSwitchThreshold$ is the minimum.
\newline
\newline
Scheduler sleep time is calculated as:
\begin{equation}
clamp(ts_i, ctxSwitchThreshold, timerPeriod)
\end{equation}
\begin{exampleblock}{}
$timerPeriod$, indirectly, controls the number of context switches but at the cost of response times.  Higher value $\implies$ fewer context switches but laggy display.
\newline
$ctxSwitchThreshold$ is useful for capping the number of context switches.
\end{exampleblock}
\end{frame}

\subsection{Command-based scheduling}

\begin{frame}[fragile]{Command-based scheduling}
\begin{itemize}
\item Instead of using timeslices, use GPU commands as budget.
\item Each GPU engine gets its own command budget.
\item Try to switch context as soon as GPU becomes idle.
\begin{exampleblock}{}
Minimize GPU idleness $\implies$ maximize throughput.
\end{exampleblock}
\begin{alertblock}{}
The costs of GPU commands are highly variable and, hence, it is hard to predict the time that will be taken.  This disturbs the overall responsiveness of the vGPUs.
\end{alertblock}
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Command-based scheduling -- Command Allocation}

For each \textit{engine} of vGPU, command budget ($b_i$) of a vGPU with weight $w_i$ is given by :
\begin{equation}
b_i = \left( allocationPeriod \times cmdsAllowed(engine) \right) \times \frac { w_i } { \sum_{i=0}^{n} w_i }
\end{equation}
where,
\\
$allocationPeriod$ is the time period for budget allocation,
\\
$cmdsAllowed$ is the max commands allowed for this engine in one cycle
\end{frame}

\begin{frame}{Command-based scheduling -- Commands committed in one cycle}
Max $cmdsAllowed$ commands are allowed to be committed in one cycle.
\\
A minimum of $minCmdThresh$ commands are still allowed even if $b_i < 0$
\newline
\newline
Hence, commands committed in one cycle is given by:
\begin{equation}
clamp(b_i, minCmdThresh, cmdsAllowed)
\end{equation}
\end{frame}


% \begin{frame}{Command-based scheduling -- Callbacks on notify interrupts}
% \begin{itemize}
% \item Callbacks are put in ring-notify interrupts from GPU which signal end of batchbuffer execution.
% \item Scheduler is invoked after checking ring status (and $ctxSwitchThreshold$).
% \item $timerPeriod$ is also used but its value is kept higher than that used in timeslice based scheduler.
% \end{itemize}
% \end{frame}

\section{Experimental Evaluation}

\subsection{Setup}

\begin{frame}[fragile]{Experimental Setup}
\begin{itemize}
	\item \textit{HW}: Intel Core i7-4770 @ 3.40GHz ``Haswell'' CPU with integrated GPU and 16 GiB memory
	\item \textit{Hypervisor}: GVT-g Xen (based on Xen 4.6)
	\item \textit{Guest configuration}: 2 VMs with 1 vCPU and 2 GiB memory assigned to each VM.
	\item \textit{Guest OS}: GVT-g Kernel (based on Linux 4.3.0-rc6) with our modifications and 64-bit Ubuntu 16.04 userspace
\end{itemize}
\begin{block}{Benchmarks:} \textbf{Lightsmark} and \textbf{OpenArena} from Phoronix Test Suite \cite{pts}
	\begin{itemize}
	\item 3D workloads with periodic bursts
	\item Provide single figure of merit: Frames per Second (FPS)
	\end{itemize}
\end{block}
\end{frame}

\subsection{Experiments}

\begin{frame}[fragile]{Weight variation: Time-based scheduling}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../weight_scaling_tbs_printer.pdf}
    \caption{\scriptsize Lightsmark: FPS vs weight for 2 VMs with scheduler time periods $5,\ 10,$ and $15$ ms.  Weight variation along X-axis; FPS along Y-axis.}
    \label{fig:weight-scaling-tbs}
\end{figure}

% \begin{figure}
%     \centering
%     \includegraphics[width=\textwidth]{../weight_scaling_tbs_oa_printer.pdf}
%     \caption{\scriptsize OpenArena: FPS vs weight for 2 VMs with scheduler time periods $5,\ 10,$ and $15$ ms.  Weight variation along X-axis; FPS along Y-axis.}
%     \label{fig:weight-scaling-tbs-oa}
% \end{figure}
\end{frame}

\begin{frame}[fragile]{Weight variation: Command-based scheduling}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../weight_scaling_cbs_printer.pdf}
    \caption{\scriptsize Lightsmark: FPS vs weight for 2 VMs.  Weight variation along X-axis; FPS along Y-axis.}
    \label{fig:weight-scaling-cbs}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{GPU share distribution}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../share_tbs_printer.pdf}
    \caption{\scriptsize Time-based scheduling: GPU share \% of 3 VMs with weights $(25, 50, 100)$. Workload: Lightsmark on VM-3, OpenArena on VM-2.}
    \label{fig:share-tbs}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../share_cbs_hetero_printer.pdf}
    \caption{\scriptsize Command-based scheduling: GPU share \% of 3 VMs with weights $(25, 50, 100)$.  Workload: Lightsmark on VM-4, OpenArena on VM-2.}
    \label{fig:share-cbs}
\end{figure}
\end{frame}

\section{Conclusion}
\begin{frame}[fragile]{Conclusion}
\begin{itemize}
	\item We presented two different approaches to scheduling of vGPUs.
	\item We were able to provide isolation and proportional fairness among the vGPUs.
	\item With time-based scheduling, we provided consistent response time.
	\item With command-based scheduling, we were able to maximize our throughput.
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
\huge Thank you
\end{center}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{References}
	\printbibliography
\end{frame}

\begin{frame}{Bonus: Programming model of Intel GPUs}
\begin{itemize}
\item Multiple Execution engines
	\begin{itemize}
	\item ``Haswell'' engines: render, blitter, video decode and video encode.
	\end{itemize}
\item Ring buffer
	\begin{itemize}
	\item A ring buffer per engine for command submission
	\item Allocated by driver during initialization.
	\item Limited in size.
	\end{itemize}
\item Command Submission:
	\begin{itemize}
	\item Asynchronous producer-consumer model
	\item Each ring buffer has two pointers: \textit{head and tail}
	\item \textit{tail} -- updated by CPU after inserting commands in the ring buffer
	\item \textit{head} -- updated by GPU after finishing execution
	\end{itemize}
\item Batch buffer
	\begin{itemize}
	\item For userspace command submission.  No size limit.
	\item Ring buffer contains pointer to batch buffer.
	\item Completion is signalled by GPU.
	\end{itemize}
\end{itemize}
\end{frame}

\end{document}

\chapter{Experimental Evaluation}
\label{chap:experiment}

Our schedulers are implemented as part of the GVT-g kernel module.  In the following sections, we describe some experiments we conducted to test the effectiveness of our prototype implementation.  We start by discussing the setup we use for our experiments.

\section{Experimental Setup}

Our experimental setup consists of an 8 core ``Haswell'' Intel(R) Core(TM) i7-4770 @ 3.40GHz CPU with integrated GPU, 16 GB memory running GVT-g's modified Xen and kernel along with our driver modifications.  Our userspace is 64-bit Ubuntu 16.04.  The guest VMs are running images from GVT-g live image distribution with Xubuntu 16.04 userspace. In our experiments we use two such guest VMs.  We use the Lightsmark and the OpenArena 3D workloads from the Phoronix test suite \cite{pts} in our experiments.

\section{Experiments and Results}

Dom0 was kept ``idle'' during the experiments.  The only workloads running on it were the Ubuntu's standard window manager and the virtual VGA output windows of the two VMs.

\subsection*{Proportional fairness and isolation: varying the weights}

Our first experiment tests the effectiveness of \textit{weight} for controlling a vGPU's performance and enforcing isolation among them.  \textit{Weight} is a relative measure of allocation hence we compare the performance of the VMs by adjusting the weights in relative proportions.  We use the Lightsmark benchmark for measuring performance which gives us a single convenient number, the average FPS (frames per second), for our use.  We run the Lightsmark benchmark simultaneously on both VMs and record the resulting FPS.

We first test \textbf{time-based} scheduling (TBS) policy.  Figure \ref{fig:weight-scaling-tbs} shows the FPS vs weight graph for the same.  Apart from varying weight, we also vary the time period of the scheduler.  As can be observed from the graph, time period has very little effect on the FPS for the range selected.  We argue that the primary reason for this is the burst nature of the GPU workload submitted by Lightsmark.  The number of command bursts completed per unit determine the FPS.  Since the total time allocated to a VM remain independent of time period, the resulting FPS is also independent of it.

Coming to scaling of FPS, we observe linear scaling from ``$3,3 (25)$'' to ``$50, 50 (25)$'' (these numbers represent the weights of the VMs in format ``VM1, VM2 (dom0)'') after which the curve flattens out.  The FPS increases linearly as weights of the two VMs are increased together giving them more on-GPU time.   In the last 4 bars, we vary the weights so that they are in proportions $1:2:4$ and $1:1:2$.  We see that the FPS almost doubles in each case.  $1:1:2$ takes some hit on FPS due to dom0 stealing some GPU cycles for rendering the desktop.

\begin{sidewaysfigure}[!htbp]
    \centering
    \includegraphics[width=\textwidth]{weight_scaling_tbs_printer.pdf}
    \caption{Time base scheduling: Scaling of FPS per weight change for 2 guest VMs.  Along X-axis, we have weights in the format $W_{vm1}, W_{vm2} (W_{dom0})$; corresponding to each VM we have 3 scheduler period values: $5ms,\ 10ms\ and\ 15ms$.  Y-axis shows the FPS recorded in Lightsmark benchmark.}
    \label{fig:weight-scaling-tbs}
\end{sidewaysfigure}

\begin{sidewaysfigure}[!htbp]
    \centering
    \includegraphics[width=\textwidth]{weight_scaling_cbs_printer.pdf}
    \caption{Command-based scheduling: Scaling of FPS per weight change for 2 guest VMs.  Along X-axis, we have weights in the format $W_{vm1}, W_{vm2} (W_{dom0})$.  Y-axis shows the FPS recorded in Lightsmark benchmark.}
    \label{fig:weight-scaling-cbs}
\end{sidewaysfigure}

Figure \ref{fig:weight-scaling-cbs} shows the weight vs FPS variation for \textbf{command-based} scheduling (CBS).  Similar to TBS, scaling is linear for some part of the curve. However, the curve flattens out earlier than TBS at ``$25, 25 (25)$''.  We postulate that this happens because the command surplus occurs at this point and after that the additional command budget allocated remains unused.

Note that we get more FPS (hence, performance) under CBS than TBS.  This disparity is because under TBS we have to wait for the timer to expire before we can schedule again even if there is no workload to be submitted.  In CBS, this does not happen.  Hence, the time wasted idling is used for gaining performance.  We validate this assertion in later experiments.

\subsection*{Testing fairness using other factors}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\textwidth]{timeline_plot.pdf}
    \caption{Time-based scheduling: Timeline plot showing occupancy of the GPU by each VM.  Workload: Lightsmark on guests, normal desktop on dom0.}
    \label{fig:timeline-plot-tbs}
\end{figure}

\Fref{fig:timeline-plot-tbs} shows the GPU occupancy timeline plot.  Each bar is associated with a VM and the width is proportional to context time.  Reading from left to right we can see that initially VM-0 is the GPU owner, followed by VM-1 and VM-2 after which the pattern repeats.  In some cases, the order breaks and a VM misses its turn, e.g. VM-2 misses its turn at 160 ms.  This happens when either the VM has an empty ring buffer or its budget is not positive.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\textwidth]{distribution.pdf}
    \caption{Time-based scheduling: Histogram of on-GPU times for 3 VMs with weights $(75, 75, 25)$.  Y-axis shows the number of contexts with given time slice.  Boxes in the top-right corner show the average context time, range and total contexts scheduled during the measurement.  Workload: Lightsmark on guests, normal desktop on dom0.}
    \label{fig:time-dist-tbs}
\end{figure}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\textwidth]{distribution_cbs.pdf}
    \caption{Command-based scheduling: Histogram of on-GPU times for 3 VMs with weights $(15, 15, 5)$.  Y-axis shows the number of contexts with given time slice.  Boxes in the top-right corner show the average context time, range and total contexts scheduled during the measurement.  Workload: Lightsmark on guests, normal desktop on dom0.}
    \label{fig:time-dist-cbs}
\end{figure}

 However, this doesn't affect the ``overall'' fairness of GPU time distribution.  Figure \ref{fig:time-dist-tbs} shows 3 histograms depicting the on-GPU time distribution (for a single context) of 3 VMs with the time-based scheduler.  The guest VMs having same weight have identical distribution.  In addition to this, the aggregate factors, namely mean time of a context (14.77 and 14.79) and the number of context switches (1704 and 1697), are almost same.  We are also able to maintain proportional relationship between the mean context switch times and weights, $14.77 / 4.95 \approx 14.79 / 4.95 \approx 3 = 75 / 25$.


Figure \ref{fig:time-dist-cbs} shows the same plot obtained from under the command-based scheduler.  The plot gives insight into the working of the schedulers.  In TBS, the number of context switches is almost same for each VM, but in CBS we see that VM-0 has fewer switches (1903 vs 3868 and 3868) than the other two VMs.  This is because in CBS we buffer GPU commands and do not schedule a vGPU if its ring buffer is empty which results in batching of commands which are submitted at once.  So, instead of submitting the commands separately in different contexts, we submit them at once thus getting fewer context switches but higher latency and poor responsiveness.

\subsection*{Comparison of predictable responsiveness}

Figure \ref{fig:share-tbs} shows a timeline plot of variation of \% GPU share of 3 VMs running with time-based scheduling (TBS).  The sampling occurs at every 200ms and we record the percentage of time assigned to VM in that period.  Figure \ref{fig:share-cbs} shows the same plot for command-based scheduling (CBS).  The difference is easily noticeable.  TBS behaves predictably with fairness maintained even in small intervals.  CBS, however, takes much more time than TBS to maintain fairness.  This effect can be observed in benchmarks with frame-rates taking a dip at certain points while skyrocketing at others.  These variations do not show up in the ``average FPS'' results.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\textwidth]{share_tbs_printer.pdf}
    \caption{Time-based scheduling: GPU share \% of 3 VMs with weights $(25, 50, 100)$. Workload: Lightsmark on VM-3, OpenArena on VM-2, normal desktop on VM-0 (dom0).}
    \label{fig:share-tbs}
	% overhead of 2.84%
\end{figure}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\textwidth]{share_cbs_hetero_printer.pdf}
    \caption{Command-based scheduling: GPU share \% of 3 VMs with weights $(25, 50, 100)$.  Workload: Lightsmark on VM-4, OpenArena on VM-2, normal desktop on VM-0 (dom0).}
    \label{fig:share-cbs}
\end{figure}

% \begin{figure}[!htbp]
%     \centering
%     \includegraphics[width=\textwidth]{share_cbs_homo.pdf}
%     \caption{Command based scheduling: GPU share \% of of 3 VMs with weights $(25, 50, 100)$. Homogeneous workload: OpenArena on guests, normal desktop on dom0.}
% 	% overhead of 2.84%
% \end{figure}
